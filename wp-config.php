<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vitacimin');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5y=x-R}.t|#^qVB}KS&S,Tb^M8*NIh{g|[+F_d6jd[RxK;5@(./`sm &Z}zY4?$L');
define('SECURE_AUTH_KEY',  '7j(oHQZbL]|=rz|[+bg!>re7k0c7Yu_dN^`4*&Q[_I}.+t^:plOM AQ+p3&7h]i>');
define('LOGGED_IN_KEY',    ']zB{a{qUfC--=F-$mR.?Ps4bS$$A,$X{42|TE*gfJG4h%705`c.GIl|f3+M:H2NX');
define('NONCE_KEY',        '|QagXYO_(maG9+WO8+Jw#W^28|1B-@q}p6ZHKsd+j-qegvaoX~@9|WobG#33z<ls');
define('AUTH_SALT',        '};g+u+$sXji*:[9>AWa3E>sd_WJ= +|JdP?7ynh~9#.B;7O|2{0cW#-|a#Yy+ZPf');
define('SECURE_AUTH_SALT', '_6(F-Z[p|+IM)L3>KIX>-@oBrCBkA.Jz}pj/(4U({VzSRj1i}uDG[;{-hPKw#XYr');
define('LOGGED_IN_SALT',   's0h$,s{ch)@R~=R7Q#[qd+|oH7cETU|kJ}V@/I*Uz0bTD0[A=kCome&-@xD^-+;K');
define('NONCE_SALT',       'VsbfnAvn2a<_!)a,zzbGRf~4$Zr$sO?+-1m:M%y{Xgh<qG{-VrJ0j+T^[h/><nxi');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
