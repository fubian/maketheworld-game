<?php

function wt_register_meta_scripts($hook_suffix) {
   if( 'post.php' == $hook_suffix || 'post-new.php' == $hook_suffix ) {
	wp_enqueue_script('wt_upload', get_template_directory_uri() .'/framework/settings/js/upload.js', array('jquery'));
     //wp_enqueue_style( 'custom_css', get_template_directory_uri() . '/inc/meta/custom.css')
  }
}
add_action( 'admin_enqueue_scripts', 'wt_register_meta_scripts' );

/**
 * Adds post layout meta box to post edit screen
 *
 */
function wellthemes_post_meta_settings(){
	add_meta_box("wt_meta_post_sidebar_settings", "Sidebar Settings", "wt_meta_post_sidebar_settings", "post", "normal", "low");
	add_meta_box("wt_meta_post_ads_settings", "Ads Settings", "wt_meta_post_ads_settings", "post", "normal", "low");
	
	add_meta_box("wt_meta_post_sidebar_settings", "Sidebar Settings", "wt_meta_post_sidebar_settings", "page", "normal", "low");
	add_meta_box("wt_meta_post_ads_settings", "Ads Settings", "wt_meta_post_ads_settings", "page", "normal", "low");
		
	$post_id = '';	
	if(isset($_GET['post'])){  
		$post_id = $_GET['post'];
    }
	
	if(isset($_POST['post_ID'])){
		$post_id =  $_POST['post_ID'];
    }	
	
	$template_file = get_post_meta($post_id, '_wp_page_template', TRUE);
	if ($template_file == 'page-featured.php') {
		add_meta_box("wt_meta_featured_page_settings", "Featured Page Settings", "wt_meta_featured_page_settings", "page", "normal", "high");
	}	
	
}
add_action( 'add_meta_boxes', 'wellthemes_post_meta_settings' );

/**
 * Display featured post settings
 *
 */ 
function wt_meta_featured_page_settings() {
	global $post;
	global $pagenow;
	
	wp_nonce_field( 'wellthemes_save_postmeta_nonce', 'wellthemes_postmeta_nonce' ); ?>
	
	<div class="meta-section">
		<h4><?php _e('Featured Section', 'wellthemes'); ?></h4>
				
		<div class="meta-field field-checkbox">			
			<input name="wt_meta_show_feat_sec1" id="wt_meta_show_feat_sec1" type="checkbox" value="1" <?php checked( get_post_meta( $post->ID, 'wt_meta_show_feat_sec1', true ), 1 ); ?> /> 
			<label for="wt_meta_show_feat_sec1"><?php _e( 'Enable Section', 'wellthemes' ); ?></label>
		</div>
		
		<div class="meta-field">
			<label for="wt_meta_sec1_title1"><?php _e( 'Title:', 'wellthemes' ); ?></label>
			<?php
				$title1 = get_post_meta( $post->ID, 'wt_meta_sec1_title1', true );
				if (empty($title1)){
					$title1 = "Today's";
				}
				
				$title2 = get_post_meta( $post->ID, 'wt_meta_sec1_title2', true );
				if (empty($title2)){
					$title2 = "Featured";
				}
			?>
			<input name="wt_meta_sec1_title1" type="text" class="compact-input" id="wt_meta_sec1_title1" value="<?php echo $title1; ?>" />
			<input name="wt_meta_sec1_title2" type="text" class="compact-input" id="wt_meta_sec1_title2" value="<?php echo $title2; ?>" />
			<div class="desc"><?php _e( 'First part will be thin, second will be bold.', 'wellthemes' ); ?></div>
		</div>
		
		<div class="meta-field">
			<label for="wt_meta_sec1_post_ids"><?php _e( 'Post IDs:', 'wellthemes' ); ?></label>
			<input name="wt_meta_sec1_post_ids" type="text" class="compact-input" id="wt_meta_sec1_post_ids" value="<?php echo get_post_meta( $post->ID, 'wt_meta_sec1_post_ids', true ); ?>" />
			<div class="desc"><?php _e( 'Enter post IDs separated by commas, eg. 2,5,7', 'wellthemes' ); ?></div>
		</div>
	
		<div class="meta-field">
			<label><?php _e( 'Category', 'wellthemes' ); ?></label>
			<select id="wt_meta_sec1_cat" name="wt_meta_sec1_cat" class="styled">
				<?php 
					$categories = get_categories( array( 'hide_empty' => 1, 'hierarchical' => 0 ) );  
					$saved_cat = get_post_meta( $post->ID, 'wt_meta_sec1_cat', true );
				?>
				<option <?php selected( 0 == $saved_cat ); ?> value="0"><?php _e('All Categories', 'wellthemes'); ?></option>	
				<?php
					if($categories){
						foreach ($categories as $category){?>
							<option <?php selected( $category->term_id == $saved_cat ); ?> value="<?php echo $category->term_id; ?>"><?php echo $category->cat_name; ?></option>							
							<?php					
						}
					}
				?>			
			</select>
			<div class="desc"><?php _e( 'If you want to display latest posts from a category.', 'wellthemes' ); ?></div>
		</div>
		
		<h4><?php _e('Featured Section 2', 'wellthemes'); ?></h4>
		
		<div class="meta-field field-checkbox">			
			<input name="wt_meta_show_feat_sec2" id="wt_meta_show_feat_sec2" type="checkbox" value="1" <?php checked( get_post_meta( $post->ID, 'wt_meta_show_feat_sec2', true ), 1 ); ?> /> 
			<label for="wt_meta_show_feat_sec2"><?php _e( 'Enable Section', 'wellthemes' ); ?></label>
		</div>
		
		<div class="meta-field">
			<label for="wt_meta_sec2_title1"><?php _e( 'Title:', 'wellthemes' ); ?></label>
			<?php
				$title1 = get_post_meta( $post->ID, 'wt_meta_sec2_title1', true );
				if (empty($title1)){
					$title1 = "Today's";
				}
				
				$title2 = get_post_meta( $post->ID, 'wt_meta_sec2_title2', true );
				if (empty($title2)){
					$title2 = "Featured";
				}
			?>
			
			<input name="wt_meta_sec2_title1" type="text" class="compact-input" id="wt_meta_sec2_title1" value="<?php echo $title1; ?>" />
			<input name="wt_meta_sec2_title2" type="text" class="compact-input" id="wt_meta_sec2_title2" value="<?php echo $title2; ?>" />
			<div class="desc"><?php _e( 'First part will be thin, second will be bold.', 'wellthemes' ); ?></div>
		</div>

		<div class="meta-field">
			<label for="wt_meta_sec2_post_ids"><?php _e( 'Post IDs:', 'wellthemes' ); ?></label>
			<input name="wt_meta_sec2_post_ids" type="text" class="compact-input" id="wt_meta_sec2_post_ids" value="<?php echo get_post_meta( $post->ID, 'wt_meta_sec2_post_ids', true ); ?>" />
			<div class="desc"><?php _e( 'Enter post IDs separated by commas, eg. 2,5,7', 'wellthemes' ); ?></div>
		</div>
		
		<div class="meta-field">
			<label><?php _e( 'Category', 'wellthemes' ); ?></label>
			<select id="wt_meta_sec2_cat" name="wt_meta_sec2_cat" class="styled">
				<?php 
					$categories = get_categories( array( 'hide_empty' => 1, 'hierarchical' => 0 ) );  
					$saved_cat = get_post_meta( $post->ID, 'wt_meta_sec2_cat', true );
				?>
				<option <?php selected( 0 == $saved_cat ); ?> value="0"><?php _e('All Categories', 'wellthemes'); ?></option>	
				<?php
					if($categories){
						foreach ($categories as $category){?>
							<option <?php selected( $category->term_id == $saved_cat ); ?> value="<?php echo $category->term_id; ?>"><?php echo $category->cat_name; ?></option>							
							<?php					
						}
					}
				?>			
			</select>
			<div class="desc"><?php _e( 'Select slider right category 1', 'wellthemes' ); ?></div>
		</div>
		
		<h4><?php _e('Featured Section Left', 'wellthemes'); ?></h4>
		<div class="meta-field">
			<?php
				$value = get_post_meta( $post->ID, 'wt_meta_sec_left_posts', true );
				if (empty($value)){
					$value = 8;
				}
			?>			
			<label for="wt_meta_sec_left_posts"><?php _e( 'Number of Posts:', 'wellthemes' ); ?></label>
			<input name="wt_meta_sec_left_posts" type="text" class="compact-input" id="wt_meta_sec_left_posts" value="<?php echo $value; ?>" />
			<div class="desc"><?php _e( 'Enter number of posts to display', 'wellthemes' ); ?></div>
		</div>
		
		<div class="meta-field">
			<label><?php _e( 'Category', 'wellthemes' ); ?></label>
			<select id="wt_meta_sec_left_cat" name="wt_meta_sec_left_cat" class="styled">
				<?php 
					$categories = get_categories( array( 'hide_empty' => 1, 'hierarchical' => 0 ) );  
					$saved_cat = get_post_meta( $post->ID, 'wt_meta_sec_left_cat', true );
				?>
				<option <?php selected( 0 == $saved_cat ); ?> value="0"><?php _e('All Categories', 'wellthemes'); ?></option>	
				<?php
					if($categories){
						foreach ($categories as $category){?>
							<option <?php selected( $category->term_id == $saved_cat ); ?> value="<?php echo $category->term_id; ?>"><?php echo $category->cat_name; ?></option>							
							<?php					
						}
					}
				?>			
			</select>
			<div class="desc"><?php _e( 'Select category for the posts', 'wellthemes' ); ?></div>
		</div>
		
	</div><!-- /meta-section -->	
		
	<div class="meta-section">
		<h4><?php _e('Carousel', 'wellthemes'); ?></h4>
		
		<div class="meta-field field-checkbox">			
			<input name="wt_meta_show_carousel" id="wt_meta_show_carousel" type="checkbox" value="1" <?php checked( get_post_meta( $post->ID, 'wt_meta_show_carousel', true ), 1 ); ?> /> 
			<label for="wt_meta_show_carousel"><?php _e( 'Enable Carousel', 'wellthemes' ); ?></label>
		</div>
		
		<div class="meta-field">
			<label><?php _e( 'Category', 'wellthemes' ); ?></label>
			<select id="wt_meta_carousel_cat" name="wt_meta_carousel_cat" class="styled">
				<?php 
					$categories = get_categories( array( 'hide_empty' => 1, 'hierarchical' => 0 ) );  
					$saved_cat = get_post_meta( $post->ID, 'wt_meta_carousel_cat', true );
				?>
				<option <?php selected( 0 == $saved_cat ); ?> value="0"><?php _e('--none--', 'wellthemes'); ?></option>	
				<?php
					if($categories){
						foreach ($categories as $category){?>
							<option <?php selected( $category->term_id == $saved_cat ); ?> value="<?php echo $category->term_id; ?>"><?php echo $category->cat_name; ?></option>							
							<?php					
						}
					}
				?>			
			</select>
			<div class="desc"><?php _e( 'Select category. Select none to display latest posts.', 'wellthemes' ); ?></div>
		</div>
	</div><!-- /meta-section -->
	
	<div class="meta-section">
		<h4><?php _e('Latest Posts', 'wellthemes'); ?></h4>
		
		<div class="meta-field field-checkbox">			
			<input name="wt_meta_show_postlist" id="wt_meta_show_postlist" type="checkbox" value="1" <?php checked( get_post_meta( $post->ID, 'wt_meta_show_postlist', true ), 1 ); ?> /> 
			<label for="wt_meta_show_postlist"><?php _e( 'Enable Latest Posts', 'wellthemes' ); ?></label>
		</div>
		
		<div class="meta-field">
			<label for="wt_meta_postlist_title1"><?php _e( 'Title:', 'wellthemes' ); ?></label>			
			<input name="wt_meta_postlist_title1" type="text" class="compact-input" id="wt_meta_postlist_title1" value="<?php echo get_post_meta( $post->ID, 'wt_meta_postlist_title1', true ); ?>" />
			<input name="wt_meta_postlist_title2" type="text" class="compact-input" id="wt_meta_postlist_title2" value="<?php echo get_post_meta( $post->ID, 'wt_meta_postlist_title2', true ); ?>" />
			<div class="desc"><?php _e( 'First part will be thin, second will be bold.', 'wellthemes' ); ?></div>
		</div>
						
		<div class="meta-field">
			<label><?php _e( 'Category', 'wellthemes' ); ?></label>
			<select id="wt_meta_postlist_cat" name="wt_meta_postlist_cat" class="styled">
				<?php 
					$categories = get_categories( array( 'hide_empty' => 1, 'hierarchical' => 0 ) );  
					$saved_cat = get_post_meta( $post->ID, 'wt_meta_postlist_cat', true );
				?>
				<option <?php selected( 0 == $saved_cat ); ?> value="0"><?php _e('--none--', 'wellthemes'); ?></option>	
				<?php
					if($categories){
						foreach ($categories as $category){?>
							<option <?php selected( $category->term_id == $saved_cat ); ?> value="<?php echo $category->term_id; ?>"><?php echo $category->cat_name; ?></option>							
							<?php					
						}
					}
				?>			
			</select>
			<div class="desc"><?php _e( 'Select category. Select none to disable.', 'wellthemes' ); ?></div>
		</div>
		
	</div><!-- /meta-section -->
				
<?php
	}
/**
 * Display sidebar settings
 *
 */
function wt_meta_post_sidebar_settings() {
	global $post;
	wp_nonce_field( 'wellthemes_save_postmeta_nonce', 'wellthemes_postmeta_nonce' );
	
	$options = get_option('wt_options');
	$sidebars = "";													
	if (isset($options['wt_custom_sidebars'])){
		$sidebars = $options['wt_custom_sidebars'] ;
	}
				
	$post_id = '';
	
	if(isset($_GET['post'])){  
		$post_id = $_GET['post'];
	}
	
	if(isset($_POST['post_ID'])){
		$post_id =  $_POST['post_ID'];
	}
	
	$template_file = get_post_meta($post_id, '_wp_page_template', TRUE);
	
	?>
		<div class="meta-field">
			<?php $saved_left_sidebar = get_post_meta( $post->ID, 'wt_meta_sidebar_left', true ); ?>
					
			<div class="meta-field">
				<label><?php _e( 'Left Sidebar:', 'wellthemes' ); ?></label>
				<select id="wt_meta_sidebar_left" name="wt_meta_sidebar_left" class="styled">
					<option <?php selected( "" == $saved_left_sidebar ); ?> value=""><?php _e('Default', 'wellthemes'); ?></option>	
					<?php
						if($sidebars){
							foreach ($sidebars as $sidebar){?>
								<option <?php selected( $sidebar == $saved_left_sidebar ); ?> value="<?php echo $sidebar; ?>"><?php echo $sidebar ?></option><?php					
							}
						}
					?>		
				</select>
			</div>				
		</div>
	
	
	<?php if ($template_file == 'page-featured.php') { ?>
		<div class="meta-field">
			<?php $saved_right_sidebar = get_post_meta( $post->ID, 'wt_meta_sidebar_right', true ); ?>
					
			<div class="meta-field">
				<label><?php _e( 'Right Sidebar:', 'wellthemes' ); ?></label>
				<select id="wt_meta_sidebar_right" name="wt_meta_sidebar_right" class="styled">
					<option <?php selected( "" == $saved_right_sidebar ); ?> value=""><?php _e('Default', 'wellthemes'); ?></option>	
					<?php
						if($sidebars){
							foreach ($sidebars as $sidebar){?>
								<option <?php selected( $sidebar == $saved_right_sidebar ); ?> value="<?php echo $sidebar; ?>"><?php echo $sidebar ?></option><?php					
							}
						}
					?>		
				</select>
				<span class="desc"><?php _e( 'You can create custom sidebars in WellThemes\'s theme options page.', 'wellthemes' ); ?></span>
			</div>
					
		</div>
	<?php } ?>
	
	<?php
}

/**
 * Display Ads settings
 *
 */ 
function wt_meta_post_ads_settings() {
	global $post;
	wp_nonce_field( 'wellthemes_save_postmeta_nonce', 'wellthemes_postmeta_nonce' );	?>
	
	<div class="meta-field textarea-field">
		<label><?php _e( 'Post Top Banner:', 'wellthemes' ); ?></label>
		<textarea name="wt_meta_banner1" id="wt_meta_banner1" type="textarea" cols="100%" rows="3"><?php echo get_post_meta( $post->ID, 'wt_meta_banner1', true ); ?></textarea>
		<div class="desc"><?php _e( 'Paste the banner code for post top banner. Leave blank to disable.', 'wellthemes' ); ?></div>			
	</div>
	
	<div class="meta-field textarea-field">
		<label><?php _e( 'Post Bottom Banner:', 'wellthemes' ); ?></label>
		<textarea name="wt_meta_banner2" id="wt_meta_banner2" type="textarea" cols="100%" rows="3"><?php echo get_post_meta( $post->ID, 'wt_meta_banner2', true ); ?></textarea>
		<div class="desc"><?php _e( 'Paste the banner code for post top banner. Leave blank to disable.', 'wellthemes' ); ?></div>			
	</div>
	
	<?php
	}
	
/**
 * Save post meta box settings
 *
 */
function wt_post_meta_save_settings() {
	global $post;
	
	if( !isset( $_POST['wellthemes_postmeta_nonce'] ) || !wp_verify_nonce( $_POST['wellthemes_postmeta_nonce'], 'wellthemes_save_postmeta_nonce' ) )
		return;

	if( !current_user_can( 'edit_posts' ) )
		return;
	
	if ( isset( $_POST['wt_meta_tiles_cat'] )){
		update_post_meta( $post->ID, 'wt_meta_tiles_cat', $_POST['wt_meta_tiles_cat'] );	
	}
	
	if ( isset( $_POST['wt_meta_show_feat_sec1'] ) && $_POST['wt_meta_show_feat_sec1'] == 1) {
		update_post_meta( $post->ID, 'wt_meta_show_feat_sec1', 1 );	
	} else {
		delete_post_meta( $post->ID, 'wt_meta_show_feat_sec1');	
	}
	
	if ( isset( $_POST['wt_meta_show_postlist'] ) && $_POST['wt_meta_show_postlist'] == 1) {
		update_post_meta( $post->ID, 'wt_meta_show_postlist', 1 );	
	} else {
		delete_post_meta( $post->ID, 'wt_meta_show_postlist');	
	}	
	
	if(isset($_POST['wt_meta_sec1_title1'])){
		update_post_meta($post->ID, 'wt_meta_sec1_title1', sanitize_text_field($_POST['wt_meta_sec1_title1']));
	}
	
	if(isset($_POST['wt_meta_sec1_title2'])){
		update_post_meta($post->ID, 'wt_meta_sec1_title2', sanitize_text_field($_POST['wt_meta_sec1_title2']));
	}
	
	if(isset($_POST['wt_meta_sec1_post_ids'])){
		update_post_meta($post->ID, 'wt_meta_sec1_post_ids', sanitize_text_field($_POST['wt_meta_sec1_post_ids']));
	}	
	
	if ( isset( $_POST['wt_meta_sec1_cat'] )){
		update_post_meta( $post->ID, 'wt_meta_sec1_cat', $_POST['wt_meta_sec1_cat'] );	
	}
	
	if ( isset( $_POST['wt_meta_show_feat_sec2'] ) && $_POST['wt_meta_show_feat_sec2'] == 1) {
		update_post_meta( $post->ID, 'wt_meta_show_feat_sec2', 1 );	
	} else {
		delete_post_meta( $post->ID, 'wt_meta_show_feat_sec2');	
	}
	
	if(isset($_POST['wt_meta_sec2_title1'])){
		update_post_meta($post->ID, 'wt_meta_sec2_title1', sanitize_text_field($_POST['wt_meta_sec2_title1']));
	}
	
	if(isset($_POST['wt_meta_sec2_title2'])){
		update_post_meta($post->ID, 'wt_meta_sec2_title2', sanitize_text_field($_POST['wt_meta_sec2_title2']));
	}
	
	if(isset($_POST['wt_meta_sec2_post_ids'])){
		update_post_meta($post->ID, 'wt_meta_sec2_post_ids', sanitize_text_field($_POST['wt_meta_sec2_post_ids']));
	}
	
	if ( isset( $_POST['wt_meta_sec2_cat'] )){
		update_post_meta( $post->ID, 'wt_meta_sec2_cat', $_POST['wt_meta_sec2_cat'] );	
	}	
	
	if(isset($_POST['wt_meta_sec_left_posts'])){
		update_post_meta($post->ID, 'wt_meta_sec_left_posts', sanitize_text_field($_POST['wt_meta_sec_left_posts']));
	}
	
	if ( isset( $_POST['wt_meta_sec_left_cat'] )){
		update_post_meta( $post->ID, 'wt_meta_sec_left_cat', $_POST['wt_meta_sec_left_cat'] );	
	}	
	
	if(isset($_POST['wt_meta_postlist_title1'])){
		update_post_meta($post->ID, 'wt_meta_postlist_title1', sanitize_text_field($_POST['wt_meta_postlist_title1']));
	}
	
	if(isset($_POST['wt_meta_postlist_title2'])){
		update_post_meta($post->ID, 'wt_meta_postlist_title2', sanitize_text_field($_POST['wt_meta_postlist_title2']));
	}
			
	if ( isset( $_POST['wt_meta_postlist_cat'] )){
		update_post_meta( $post->ID, 'wt_meta_postlist_cat', $_POST['wt_meta_postlist_cat'] );	
	}
		
	if ( isset( $_POST['wt_meta_show_carousel'] ) && $_POST['wt_meta_show_carousel'] == 1) {
		update_post_meta( $post->ID, 'wt_meta_show_carousel', 1 );	
	} else {
		delete_post_meta( $post->ID, 'wt_meta_show_carousel');	
	}
	
	if ( isset( $_POST['wt_meta_carousel_cat'] )){
		update_post_meta( $post->ID, 'wt_meta_carousel_cat', $_POST['wt_meta_carousel_cat'] );	
	}
		
	if ( isset( $_POST['wt_meta_sidebar_left'] )){
		update_post_meta( $post->ID, 'wt_meta_sidebar_left', $_POST['wt_meta_sidebar_left'] );	
	}
	
	if ( isset( $_POST['wt_meta_sidebar_right'] )){
		update_post_meta( $post->ID, 'wt_meta_sidebar_right', $_POST['wt_meta_sidebar_right'] );	
	}	
			
	if(isset($_POST['wt_meta_banner1'])){
		update_post_meta( $post->ID, 'wt_meta_banner1', $_POST['wt_meta_banner1'] );
	}
	
	if(isset($_POST['wt_meta_banner2'])){
		update_post_meta( $post->ID, 'wt_meta_banner2', $_POST['wt_meta_banner2'] );
	}
	
}
add_action( 'save_post', 'wt_post_meta_save_settings' );