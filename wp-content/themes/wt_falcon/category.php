<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package  WellThemes
 * @file     category.php
 * @author   Well Themes Team
 * @link 	 http://wellthemes.com
 */
?>
<?php get_header(); ?>

<div id="content" class="post-archive">
	<?php if ( have_posts() ) : ?>
	
		<header class="archive-header">		
			<h2><?php	printf( __( 'Category Archives: %s', 'wellthemes' ), '<span>' . single_cat_title( '', false ) . '</span>' );?></h2>		
		</header>
		
		<?php
			$category_description = category_description();
			if ( ! empty( $category_description )) {
				echo apply_filters( 'category_archive_meta', '<div class="archive-desc section">' . $category_description . '</div>' );
			}
		?>
		<div class="archive-postlist">
			<?php 
				while ( have_posts() ) : the_post();										
					get_template_part( 'content', 'excerpt' );
				endwhile; 
			?>
		</div>
		<?php wt_pagination(); ?>
	
	<?php endif; ?>
</div><!-- /content -->
	
<?php get_sidebar('left'); ?>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>
