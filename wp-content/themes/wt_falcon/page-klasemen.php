<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package  WellThemes
 * @file     page.php
 * @author   Well Themes Team
 * @link 	 http://wellthemes.com
 */
 ?>
<?php get_header(); ?>

<div class="single-page">
	<header class="entry-header"> 
		<h1 style="display:inline-block;">Games</h1>
        <div class="menu-game">
        	<nav>
            	<ul>
                	<li><a href="">Hi, Temins</a></li>
                    <li class="tebak-gambar"><a href="">Tebak Gambar</a></li>
                    <li class="klasemen"><a href="">klasemen</a></li>
                    <li class="logout-game"><a href="">logout</a></li>
                </ul>
            </nav>
        </div>
	</header>
    <div class="clear"></div>
    <section id="main-game">
    	<h2 class="klasemen-tagline">Klasemen</h2>
    	<div class="top-logo-game">
        	<h2>Tebak Gambar</h2>           
            <div class="logo-tebak">
            	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-tebak.png">
            </div>
        </div>
       <table class="klasemen-table">
       	<thead>
        	<tr>
            	<td>No</td><td>Nama</td><td>Point</td><td></td>
            </tr>
        </thead>
        <tbody>
        	<tr>
            	<td>1</td><td>Chibi</td><td>100</td><td class="no-bg"><a href="#add-point" class="add-point_open normal-button">Tambah poin</a></td>
            </tr>
            <tr class="spacer"></tr> <!--harus dikasih buat kasih margin-->
            <tr class="no-bg">
            	<td>2</td><td>Chibi</td><td>100</td><td class="no-bg"><a href="" class="wait-button">Tambah poin</a></td>
            </tr>
            <tr class="spacer">
            <tr>
            	<td>3</td><td>Chibi</td><td>100</td><td class="no-bg"><a href="" class="normal-button">Tambah poin</a></td>
            </tr>
            <tr class="spacer"></tr> <!--harus dikasih buat kasih margin-->
            <tr class="no-bg">
            	<td>4</td><td>Chibi</td><td>100</td><td class="no-bg"><a href="" class="app-button">Tambah poin</a></td>
            </tr>
            <tr class="spacer">
            <tr>
            	<td>5</td><td>Chibi</td><td>100</td><td class="no-bg"><a href="" class="normal-button">Tambah poin</a></td>
            </tr>
        </tbody>
       </table>
        
    </section>
    <section class="wait-notif">
    	<span class="wait-app">menunggu approval</span>
        <span class="approval">approved</span>
    </section>

	</div><!-- /content -->
    <div id="add-point">
    	<span class="close-popup add-point_close"></span>
    	<h2 class="tagline-submit">SUBMIT FOTO</h2>
        <span class="tagline-submit-foto">Submit foto kamu bareng vitacimin  buat ngedapetin  <br /> tambahan 1000 poin</span>
    	<form action="/file-upload" class="dropzone">
          <div class="dropupload">
           		<div class="message-upload">
                </div>
          </div>
        </form>
        <input type="submit" value="submit" class="submit-login" />
        <span class="add-tagline">* poin akan ditambah setelah ada konfirmasi dari Vitacimin</span>
    </div>
   
<?php get_footer(); ?>
