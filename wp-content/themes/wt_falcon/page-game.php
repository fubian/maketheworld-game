<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package  WellThemes
 * @file     page.php
 * @author   Well Themes Team
 * @link 	 http://wellthemes.com
 */
 ?>
<?php get_header(); ?>

<div class="single-page">
	<header class="entry-header"> 
		<h1 style="display:inline-block;">Games</h1>
        <div class="menu-game">
        	<nav>
            	<ul>
                	<li><a href="">Hi, Temins</a></li>
                    <li class="tebak-gambar"><a href="">Tebak Gambar</a></li>
                    <li class="klasemen"><a href="">klasemen</a></li>
                    <li class="logout-game"><a href="">logout</a></li>
                </ul>
            </nav>
        </div>
	</header>
    <div class="clear"></div>
    <section id="main-game">
    	<div class="top-logo-game">
        	<h2>Tebak Gambar</h2>           
            <div class="logo-tebak">
            	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-tebak.png">
            </div>
        </div>
        <h2 class="tagline-login">Hi Temins, ikutan game tebak gambar yuk! <br /> Berhadiah Rp 50.000 tiap hari.</h2>
        <div class="wraper-login-section">
        	<div class="wraper-box-login" id="signupnot">
                <h2 class="login-top" id="sign">SIGN UP</h2>
                <h2 class="login-top" id="log">LOGIN</h2>
                <div id="wraper-form">
                <input type="text"  placeholder="Nama" class="input-sign-up" />
                <input type="email"  placeholder="Email" class="input-sign-up" />
                <input type="Password"  placeholder="Password" class="input-sign-up" />
                <input type="submit" value="submit" class="submit-login" />
                </div>
                 <!--hide login form-->
                <div id="login-wraper">
                <input type="email"  placeholder="Email masuk" class="input-sign-up" />
                <input type="Password"  placeholder="Password" class="input-sign-up" />
                <input type="submit" value="submit" class="submit-login" />
                </div>
                <div class="or-login">
                     atau <span id="to-login">Login</span>  <span id="to-sign">Signup</span>
                </div>
            </div>
             <div class="copy-right-game">
            	<span class="cp-game">Vitacimin 2014</span>
                <span class="tmc">Terms & Condition</span>
            </div>
         </div>
        
    </section>

	</div><!-- /content -->
<?php get_footer(); ?>