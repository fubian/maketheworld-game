<?php
/**
 * The template for displaying content in the archive and search results template
 *
 * @package  WellThemes
 * @file     content-excerpt.php
 * @author   WellThemes Team
 * @link 	 http://wellthemes.com
 */
?>
<article <?php post_class(); ?> data-animation="fadeInLeft">
	
	<?php if ( has_post_thumbnail() ) {	?>
		<div class="thumb excerpt-thumb overlay">
			<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'wt550_300' ); ?></a>			
		</div>
	<?php } ?>
	
	<div class="excerpt-wrap">
	
		<div class="entry-meta">
			<?php wt_get_first_cat(); ?>
				
			<span class="author">
				<?php _e('Author:', 'wellthemes'); ?>
				<?php the_author_posts_link(); ?> 
			</span>

			<span class="sep">-</span>														
			<span class="date"><?php echo get_the_date(); ?></span>	
															
			<?php				
				$comment_count = get_comments_number($post->ID);
				if ($comment_count > 0){ ?>	
					<span class="comments">
						<i class="fa fa-comment"></i>
						<?php comments_popup_link( __('', 'wellthemes'), __( '1', 'wellthemes'), __('%', 'wellthemes')); ?>	
					</span>					
					<?php
				}

				if ( wt_get_option( 'wt_enable_rating' ) == 1 ){
					ec_stars_rating_archive(); 
				}
			?>					
		</div>	
		
		<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>		
		<?php the_excerpt(); ?>
		
		<div class="excerpt-footer">
			<div class="more-link">
				<a href="<?php the_permalink() ?>"><?php _e('Read more', 'wellthemes'); ?></a>
			</div>
			<div class="share-links">
				<span class="share"><?php _e('Share', 'wellthemes'); ?></span>
				<span class="links">
					<a class="twitter" href="http://twitter.com/home?status=<?php echo urlencode( get_the_title() ); ?>%20<?php echo urlencode( get_the_permalink() ); ?>" target="_blank"><i class="fa fa-twitter-square"></i><?php _e('Twitter', 'wellthemes'); ?></a>	
					<a  class="fb" href="http://facebook.com/share.php?u=<?php echo urlencode( get_the_permalink() ); ?>&amp;t=<?php echo urlencode( get_the_title() ); ?>" target="_blank"><i class="fa fa-facebook-square"></i><?php _e('Facebook', 'wellthemes'); ?></a>
				</span>
			</div>
		</div>
				
	</div>
	
</article><!-- /post-<?php the_ID(); ?> -->