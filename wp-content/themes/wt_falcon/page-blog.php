<?php
/**
 * Template Name: Blog
 * Description: A Page Template to display blog archives with the sidebar.
 *
 * @package  WellThemes
 * @file     page-blog.php
 * @author   Well Themes Team
 * @link 	 http://wellthemes.com
 */
?>
<?php get_header(); ?>

<div id="content" class="archive blog-page" >
		
		<header class="entry-header">	
			<h1><?php the_title(); ?></h1>
		</header>
		
		<?php
			if ( get_query_var('paged') ) {
				$paged = get_query_var('paged');
			} elseif ( get_query_var('page') ) {
				$paged = get_query_var('page');
			} else {
				$paged = 1;
			}
			
			$args = array(
				'post_status' => 'publish',
				'ignore_sticky_posts' => 1,
				 'paged' => $paged
			);			
		
			$wp_query = new WP_Query( $args );			
			
			if ( $wp_query -> have_posts() ) : ?>
				<div class="archive-postlist">
					<?php 
						while ( $wp_query -> have_posts() ) : $wp_query -> the_post();													
							get_template_part( 'content', 'excerpt' );
						endwhile; 
					?>
				</div><?php			
				
				wt_pagination();
				wp_reset_query();
			endif; 
		?>
			
</div><!-- /content -->
<?php get_sidebar('left'); ?>
<?php get_sidebar('right'); ?>
<?php get_footer(); ?>