<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package  WellThemes
 * @file     footer.php
 * @author   Well Themes Team
 * @link 	 http://wellthemes.com
 */
?>
		</div><!-- /inner-wrap -->
	</section><!-- /main -->

	<footer id="footer">
		
		<div class="footer-widgets">
			<div class="inner-wrap">			
				<div class="footer-widget">	
					<?php
						if ( ! dynamic_sidebar( 'footer-1' ) ) : 			
						endif;
					?>
				</div>
				
				<div class="footer-widget">
					<?php
						if ( ! dynamic_sidebar( 'footer-2' ) ) : 			
						endif;
					?>
				</div>
				
				<div class="footer-widget">
					<?php
					if ( ! dynamic_sidebar( 'footer-3' ) ) : 			
					endif;				
					?>
				</div>
				
				<div class="footer-widget col-last">
					<?php
					if ( ! dynamic_sidebar( 'footer-4' ) ) : 			
					endif;					
					?>
                    <div class="widget-title">
						<h4 class="title"><span>Instagram</span></h4>
					</div>
                      <div class="instag">
                        <ul class="thumbnails no-bullet">
                          <?php
                          $instagram = new WP_Query('posts_per_page=6&category_name=instagram');
                          if($instagram->have_posts()):
                            while($instagram->have_posts()) : $instagram->the_post();
                          ?>
                          <li>
                              <a href="<?php echo get_post_meta($post->ID, "_instagram_link", true); ?>" target="_blank"><img src="<?php echo get_post_meta($post->ID, "_instagram_url", true); ?>" alt=""></a>
                          </li>
                          <?php
                            endwhile;
                          endif;
                          ?>
                        </ul>
                            </div>
                            </div>
                        </div>
                </div><!-- /footer-widgets -->	
			
		<div class="footer-info">
			<div class="inner-wrap">
				<?php if (wt_get_option( 'wt_footer_text_left' )){ ?> 
					<div class="footer-left">
						<?php echo wt_get_option( 'wt_footer_text_left' ); ?>			
					</div>
				<?php } ?>				
			</div>		
		</div>		
	</footer><!-- /footer -->

<?php wp_footer(); ?>

</body>
</html>