<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package  WellThemes
 * @file     index.php
 * @author   Well Themes Team
 * @link 	 http://wellthemes.com
 */
?>
<?php get_header(); ?>

<div id="content" class="post-archive">
	<?php if ( have_posts() ) : ?>		
		<div class="archive-postlist">
			<?php 
				while ( have_posts() ) : the_post();										
					get_template_part( 'content', 'excerpt' );
				endwhile; 
			?>
		</div>
		<?php wt_pagination(); ?>
	<?php endif; ?>		
</div>

<?php get_sidebar('left'); ?>
<?php get_sidebar('right');?>
<?php get_footer(); ?>