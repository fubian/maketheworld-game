<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package  WellThemes
 * @file     page.php
 * @author   Well Themes Team
 * @link 	 http://wellthemes.com
 */
 ?>
<?php get_header(); ?>

<div class="single-page">
	<header class="entry-header"> 
		<h1 style="display:inline-block;">Games</h1>
        <div class="menu-game">
        	<nav>
            	<ul>
                	<li><a href="">Hi, Temins</a></li>
                    <li class="tebak-gambar"><a href="">Tebak Gambar</a></li>
                    <li class="klasemen"><a href="">klasemen</a></li>
                    <li class="logout-game"><a href="">logout</a></li>
                </ul>
            </nav>
        </div>
	</header>
    <div class="clear"></div>
    <section id="main-game">
        <h2 class="tagline-game">Hi Temins, Games Tebak Gambar Vitacimin</h2>
        <p class="tagline-p">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
        <div class="wrapper-game">
            <div class="game-l">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/games-1.jpg">
            </div>

            <div class="game-r">
                <div class="game-time">
                    <h2>Waktu Tersisa</h2>
                    <p>00:00:30</p>
                </div>

                <div class="game-question">
                    <h2>Soal</h2>
                    <p>Siapakah nama pria berpakaian hijau di samping ini ?</p>
                </div>

                <div class="game-answer">
                    <h2>Jawaban</h2>
                    <input type="text"  placeholder="Jawaban" class="input-answer" />
                </div>

                <input type="submit" value="submit" class="salah-popup_open submit-answer" />
                <!-- kalo jawaban bener tinggal nambahin class "benar-popup_open" kalau salah "salah-popup_open" -->
            </div>
        </div>
    </section>

	</div><!-- /content -->
    <!--benar popup-->
    <div id="benar-popup">
    	<h2 class="main-tagline-popup">BENAR</h2>
        <span class="tagline-small-popup">kamu mendapatkan 100 poin</span>
        <span class="close-popup benar-popup_close"></span>
    </div>
     <!--salah popup-->
    <div id="salah-popup">
    	<h2 class="main-tagline-popup">SALAH</h2>
        <span class="tagline-small-popup">Silahkan Coba lagi besok</span>
        <span class="close-popup salah-popup_close"></span>
    </div>
<?php get_footer(); ?>